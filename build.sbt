name := "TinkoffInvestBot"

ThisBuild / version := "0.1"

ThisBuild / scalaVersion := "2.13.4"
import Versions._

libraryDependencies in ThisBuild ++= Seq(

  "io.circe"                   %% "circe-core"                    % circeVersion,
  "io.circe"                   %% "circe-parser"                  % circeVersion,
  "io.circe"                   %% "circe-generic"                 % circeVersion,
  "io.circe"                   %% "circe-literal"                 % circeVersion,
  "io.circe"                   %% "circe-generic-extras"          % circeVersion,
  "org.typelevel"              %% "cats-core"                     % catsCoreVersion,
  "org.typelevel"              %% "cats-effect"                   % catsEffectVersion,
  "org.tpolecat"               %% "doobie-core"                   % doobieVersion,
  "org.tpolecat"               %% "doobie-postgres"               % doobieVersion,
  "org.tpolecat"               %% "doobie-specs2"                 % doobieVersion % "test",
  "org.tpolecat"               %% "doobie-hikari"                 % doobieVersion,
  "org.http4s"                 %% "http4s-dsl"                    % http4sVersion,
  "org.http4s"                 %% "http4s-circe"                  % http4sVersion,
  "org.http4s"                 %% "http4s-blaze-client"           % http4sVersion,
  "org.http4s"                 %% "http4s-blaze-server"           % http4sVersion,
  "org.http4s"                 %% "http4s-jdk-http-client"        % jdkHttpClientVersion,
  "io.github.apimorphism"      %% "telegramium-core"              % telegramiumVersion,
  "io.github.apimorphism"      %% "telegramium-high"              % telegramiumVersion,
  "com.github.pureconfig"      %% "pureconfig"                    % pureConfigVersion,
  "com.typesafe.scala-logging" %% "scala-logging"                 % scalaLoggingVersion,
  "org.scalamock"              %% "scalamock"                     % scalaMockVersion % Test,
  "org.scalatest"              %% "scalatest"                     % scalaTestVersion % Test,
  "com.codecommit"             %% "cats-effect-testing-scalatest" % catsEffectScalatestVersion % Test,
  "com.beachape"               %% "enumeratum-circe"              % enumeratumCirceVersion,
)

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.11.1" cross CrossVersion.full)
