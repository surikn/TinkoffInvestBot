object Versions {
  val scalaTestVersion = "3.1.0"
  val scalaCheckVersion = "1.14.0"
  val mockitoVersion = "2.26.0"

  val catsVersion = "2.1.4"
  val circeVersion = "0.13.0"
  val http4sVersion = "0.21.7"
  val doobieVersion = "0.9.0"
  val telegramiumVersion = "2.49.0"
  val pureConfigVersion = "0.13.0"
  val catsCoreVersion = "2.1.1"
  val catsEffectVersion = "2.1.4"
  val jdkHttpClientVersion = "0.3.1"
  val scalaLoggingVersion = "3.9.2"
  val scalaMockVersion = "4.4.0"
  val catsEffectScalatestVersion = "0.4.2"
  val enumeratumCirceVersion = "1.6.1"
}
