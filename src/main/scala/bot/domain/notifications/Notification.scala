package bot.domain.notifications

case class Notification(userId: Long, message: String)

