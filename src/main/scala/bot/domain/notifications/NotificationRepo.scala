package bot.domain.notifications

trait NotificationRepo[F[_]] {
  def push(notification: Notification): F[Int]
  def pull(): F[List[Notification]]
}