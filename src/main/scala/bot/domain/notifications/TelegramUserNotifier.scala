package bot.domain.notifications

import cats.effect.{Sync, Timer}
import cats.implicits._
import org.slf4j.LoggerFactory
import bot.telegram.Bot

class TelegramUserNotifier[F[_]: Sync : Timer](implicit tgbot: Bot[F],
                                               implicit val notificationRepo: NotificationRepo[F])
  extends Notifier[F] {

  override def start(): F[Unit] = {
    for {
      notifications <- notificationRepo.pull()
      _ <- notify(notifications)
    } yield ()
  }

  override def notify(notifications: List[Notification]): F[Unit] = {
    for {
      _ <- notifications.traverse {
        notification => tgbot.send(notification.userId, notification.message)
      }
    } yield ()
  }

}