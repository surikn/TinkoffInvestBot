package bot.domain.notifications

import bot.db.DBAccess
import cats.effect.Sync
import cats.implicits._
import org.slf4j.LoggerFactory

class NotificationModel[F[_]: Sync](implicit dbAccess: DBAccess[F]) extends NotificationRepo[F] {

  private val log = LoggerFactory.getLogger("NotificationModel")

  def push(notification: Notification): F[Int] = {
    for {
      result <- dbAccess.insertNotification(notification)
      r <- result match {
        case Left(e) => Sync[F].delay(log.error(s"$e")) >>
          e.getErrorCode.pure[F]
        case Right(value) => value.pure[F]
      }
    } yield r
  }


  def pull(): F[List[Notification]] = {
    for {
      result <- dbAccess.getNotifications
      r <- result match {
        case Left(e) => Sync[F].delay(log.error(s"$e")) >>
          List().pure[F]
        case Right(value) => value.pure[F]
      }
    } yield r
  }
}
