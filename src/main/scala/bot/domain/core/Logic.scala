package bot.domain.core

import api.entities.Operation
import bot.domain.core.controllers.{MarketController, MarketInstrumentController, OrderController, PortfolioController}


trait Logic[F[_]] {
  this: MarketController[F] with MarketInstrumentController[F] with OrderController[F] with PortfolioController[F] =>
  def helpMsg(): F[String]
  def portfolioMsg(): F[String]
  def marketInstrumentMsg(instrument: String): F[String]
  def doOrderbook(args: String): F[String]
  def doCancelOrder(args: String): F[String]
  def doLimitOrder(operation: Operation, text: String): F[String]
  def doMarketOrderCmd(operation: Operation, args: String, userId: Long = 0): F[String]
  def activeOperations: F[String]
  def stopOperations: F[String]
}

