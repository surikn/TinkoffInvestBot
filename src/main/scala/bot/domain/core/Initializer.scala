package bot.domain.core

import api.websocket.client.TInvestWSApi
import api.entities.CandleResolution
import bot.db.DBAccess
import bot.domain.OperationStatus
import cats.effect.Sync
import org.slf4j.Logger
import cats.implicits._

class Initializer[F[_]](val log: Logger)(implicit
                                         dbAccess: DBAccess[F],
                                         tinvestWSApi: TInvestWSApi[F],
                                         sync: Sync[F]) {
  def init(): F[Unit] = {
    for {
      activeOpsE <- dbAccess.getOpsByStatus(OperationStatus.Active)
      _ <- activeOpsE match {
        case Left(e) => Sync[F].delay(log.error(s"$e"))
        case Right(activeOps) => activeOps.map {
          op => {
            Sync[F].delay(log.info(s"${op.figi} ${op.operationStatus}"))
            op.figi
          }
        }.distinct.traverse {
          figi => tinvestWSApi.subscribeCandle(figi, CandleResolution.`1min`) >>
            Sync[F].delay(log.info(s"subscribeCandle: $figi"))
        }
      }
    } yield ()
  }
}
