package bot.domain.core.controllers

import api.entities.Orders.MarketOrderRequest
import api.entities.{CandleResolution, FIGI, Operation, PlacedOrder}
import api.rest.TInvestRESTApi
import api.websocket.client.TInvestWSApi
import bot.db.DBAccess
import bot.domain.{BotOperation, OperationStatus}
import cats.effect.Sync
import cats.implicits._
import org.slf4j.Logger


trait MarketController[F[_]] {
  def doMarketOrderCmd(operation: Operation, args: String, userId: Long = 0): F[String]

  def activeOperations: F[String]

  def stopOperations: F[String]
}


class MarketControllertImpl[F[_] : Sync](val log: Logger)(implicit tInvestRESTApi: TInvestRESTApi[F],
                                                          tInvestWSApi: TInvestWSApi[F],
                                                          dbAccess: DBAccess[F]) extends MarketController[F] {
  // MarketLogic
  private def parseMarketOrderArgs(args: Array[String]): Option[(Int, String)] = {
    val figi = args(1)
    args(2).toIntOption match {
      case lots if lots.isDefined => Some(lots.get, figi)
      case _ => None
    }
  }

  private def parseSmartMarketOrderBuyArgs(args: Array[String]): Option[(String, Int, Double, Double)] = {
    val figi = args(1)
    (args(2).toIntOption, args(3).toDoubleOption, args(4).toDoubleOption) match {
      case (lots, sloss, tprofit) if lots.isDefined && sloss.isDefined && tprofit.isDefined =>
        Some(figi, lots.get, sloss.get, tprofit.get)
      case _ => None
    }
  }

  def doMarketOrderCmd(operation: Operation, args: String, userId: Long = 0): F[String] = {
    val sArgs = args.filter(c => c != '/' || c != ' ').split('.')
    sArgs.length match {
      case 3 => doSimpleMarketOrder(operation, sArgs)
      case 5 => doSmartMarketOrderBuy(userId, sArgs)
      case _ => "Wrong command".pure[F]
    }
  }

  private def doSimpleMarketOrder(operation: Operation, args: Array[String]): F[String] = {
    val parsedArgs = parseMarketOrderArgs(args)
    parsedArgs match {
      case None => s"Wrong command".pure[F]
      case Some(args: (Int, String)) =>
        val (lots, figi) = args
        for {
          result <- tInvestRESTApi.marketOrder(FIGI(figi), MarketOrderRequest(lots, operation))
          reply = result match {
            case Right(r) => s"`Success: orderId - ${r.payload.orderId}`"
            case Left(e) => {
              log.error(e.toString)
              s"`Error: ${e.status}`"
            }
          }
        } yield reply
    }
  }

  private def doSmartMarketOrderBuy(userId: Long, args: Array[String]): F[String] = {
    val parsedArgs = parseSmartMarketOrderBuyArgs(args)
    parsedArgs match {
      case None => s"Wrong command".pure[F]
      case Some(args: (String, Int, Double, Double)) =>
        val (figi, lots, stopLoss, takeProfit) = args
        for {
          checkRes <- checkStopLossAndTakeProfit(figi, stopLoss, takeProfit)
          msg <- checkRes match {
            case Left(e) => e.pure[F]
            case Right(checkMsg) =>
              for {
                orderE <- tInvestRESTApi.marketOrder(FIGI(figi), MarketOrderRequest(lots, Operation.Buy))
                msg <- orderE match {
                  case Left(e) =>
                    Sync[F].delay(log.error(e.toString)) >>
                      s"`Error: ${e.status}``".pure[F]
                  case Right(order) => for {
                    _ <- registerOperation(FIGI(figi), stopLoss, takeProfit, userId, order.payload)
                    msg <- s"""|$checkMsg
                               |Выполнена покупка акций $figi, количество $lots
                               |""".stripMargin.pure[F]
                  } yield msg
                }
              } yield msg
          }
        } yield msg
    }
  }


  private def checkStopLossAndTakeProfit(figi: String,
                                         stopLoss: Double,
                                         takeProfit: Double)
  : F[Either[String, String]] = {
    for {
      orderBookE <- tInvestRESTApi.orderbook(FIGI(figi), 1)
      result <- orderBookE match {
        case Left(e) =>
          Sync[F].delay(log.error(e.toString)) >>
            Left(s"`Error: ${e.status}`").pure[F]
        case Right(orderBook) =>
          orderBook.payload.lastPrice match {
            case None => Left(s"Не удалось получить текущую стоимость для $figi").pure[F]
            case Some(lastPrice) =>
              if (lastPrice <= stopLoss) Left(s"StopLoss($stopLoss) выше чем стоимость акции ($lastPrice)").pure[F]
              else if (lastPrice >= takeProfit) Left(s"TakeProfit($takeProfit) ниже чем стоимость акции ($lastPrice)").pure[F]
              else Right(s"Текущая стоимость акции ($lastPrice)").pure[F]
          }
      }
    } yield result
  }

  private def registerOperation(figi: FIGI,
                                stopLoss: Double,
                                takeProfit: Double,
                                userId: Long,
                                order: PlacedOrder)
  : F[Unit] = {
    val operation = BotOperation(
      None,
      figi,
      stopLoss,
      takeProfit,
      OperationStatus.Active,
      order.orderId,
      order.status,
      order.operation,
      order.requestedLots,
      order.executedLots,
      userId
    )
    for {
      currentOpsE <- dbAccess.getOpsByStatus(OperationStatus.Active)
      _ <- currentOpsE match {
        case Left(e) => Sync[F].delay(log.error(s"$e"))
        case Right(currentOps) => {
          /* Делаем подписку на свечи только при условии если подписка на figi отсутствует */
          val subscribed = currentOps.exists(_.figi == figi)
          if (subscribed) {
            Sync[F].delay(log.warn(s"This $figi was previously subscribed"))
          } else {
            tInvestWSApi.subscribeCandle(figi, CandleResolution.`1min`) >> // TODO: Что если тут произойдет ошибка?
              Sync[F].delay(log.info(s"subscribeCandle: $figi"))
          }
        }
      }
      _ <- dbAccess.insertOperation(operation) // TODO: Что если тут произойдет ошибка?
    } yield ()
  }

  def activeOperations: F[String] = {
    for {
      opsE <- dbAccess.getOpsByStatus(OperationStatus.Active)
      msg <- opsE match {
        case Left(e) => s"${e.getMessage}".pure[F]
        case Right(ops) => {
          if (ops.isEmpty) {
            s"Список активных операций пуст".pure[F]
          } else {
            ops.map {
              op =>
                s"`UserID[${op.tgUserId}] [${op.id.getOrElse(-1)}] ${op.figi} " +
                  s"Lots=${op.executedLots} TakeProfit=${op.takeProfit} StopLoss=${op.stopLoss}`\n"
            }.mkString("").pure[F]
          }
        }
      }
    } yield msg
  }

  def stopOperations: F[String] = {
    for {
      opsE <- dbAccess.getOpsByStatus(OperationStatus.Active)
      results <- opsE match {
        case Left(e) => Seq(s"${e.getMessage}").pure[F]
        case Right(ops) => {
          if (ops.isEmpty) {
            Seq(s"Список активных операций пуст").pure[F]
          } else {
            ops.traverse {
              op =>
                op.id match {
                  case None => s"Wrong operation id".pure[F]
                  case Some(id) => {
                    for {
                      _ <- dbAccess.updateOperationStatus(id, OperationStatus.Stop) // TODO: Что если тут произойдет ошибка?
                      _ <- tInvestWSApi.unsubscribeCandle(op.figi, CandleResolution.`1min`) // TODO: Что если тут произойдет ошибка?
                      _ <- Sync[F].delay(log.info(s"Stop operation with id[$id]"))
                      msg <- s"`Stop $id ${op.figi} ${op.stopLoss} ${op.takeProfit}`".pure[F]
                    } yield msg
                  }
                }
            }
          }
        }
      }
      msg <- results.mkString("\n").pure[F]
    } yield msg
  }
}