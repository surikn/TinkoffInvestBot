package bot.domain.core.controllers

import api.rest.TInvestRESTApi
import cats.effect.Sync
import org.slf4j.Logger
import cats.implicits._

trait PortfolioController[F[_]] {
  def portfolioMsg(): F[String]
}


class PortfolioControllerImpl[F[_] : Sync](val log: Logger)(implicit tInvestRESTApi: TInvestRESTApi[F])
  extends PortfolioController[F] {
  def portfolioMsg(): F[String] = {
    for {
      portfolio <- tInvestRESTApi.getPortfolio
      _ <- Sync[F].delay(log.info(portfolio.toString))
      msg <- portfolio match {
        case Right(p) => s"${
          p.payload.positions.sortBy(_.instrumentType).map {
            pos => s"`${pos.figi} ${pos.instrumentType} [${pos.name}] balance ${pos.balance}`"
          }.mkString("\n")
        }".pure[F]
        case Left(e) =>
          Sync[F].delay(log.error(e.toString)) >>
            s"Error: ${e.status}".pure[F]
      }
    } yield msg
  }
}