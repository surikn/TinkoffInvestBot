package bot.domain.core.controllers

import api.rest.TInvestRESTApi
import cats.effect.Sync
import org.slf4j.Logger
import cats.implicits._


trait MarketInstrumentController[F[_]] {
  def marketInstrumentMsg(instrument: String): F[String]
}

class MarketInstrumentControllerImpl[F[_] : Sync](val log: Logger)(implicit tInvestRESTApi: TInvestRESTApi[F])
  extends MarketInstrumentController[F] {

  def marketInstrumentMsg(instrument: String): F[String] = {
    for {
      currencies <- instrument match {
        case "stocks" => tInvestRESTApi.stocks()
        case "bonds" => tInvestRESTApi.bonds()
        case "etfs" => tInvestRESTApi.etfs()
        case "currencies" => tInvestRESTApi.currencies()
      }
      _ <- Sync[F].delay(log.info(currencies.toString))
      msg <- currencies match {
        case Right(p) => s"${
          p.payload.instruments.map {
            pos => s"`${pos.figi} ${pos.name}`"
          }.mkString("\n")
        }".pure[F]
        case Left(e) => s"`Error: ${e.status}`".pure[F]
      }
    } yield msg
  }
}
