package bot.domain.core.controllers

import api.entities.{FIGI, Operation}
import api.entities.Orders.LimitOrderRequest
import api.rest.TInvestRESTApi
import cats.effect.Sync
import org.slf4j.Logger
import cats.implicits._

trait OrderController[F[_]] {
  def doOrderbook(args: String): F[String]

  def doCancelOrder(args: String): F[String]

  def doLimitOrder(operation: Operation, text: String): F[String]
}

class OrderControllerImpl[F[_] : Sync](val log: Logger)(implicit tInvestRESTApi: TInvestRESTApi[F]) extends OrderController[F] {
  private def parseOrderbookArgs(text: String): Option[(Int, String)] = {
    val args = text.filter(c => c != '/' || c != ' ').split('.')
    args.length match {
      case 3 =>
        val figi = args(1)
        args(2).toIntOption match {
          case depth if depth.isDefined => Some(depth.get, figi)
          case _ => None
        }
      case _ => None
    }
  }


  def doOrderbook(args: String): F[String] = {
    val parsedArgs = parseOrderbookArgs(args)
    parsedArgs match {
      case None => s"Wrong command".pure[F]
      case Some(args: (Int, String)) =>
        val (depth, figi) = args
        for {
          result <- tInvestRESTApi.orderbook(FIGI(figi), depth)
          reply = result match {
            case Right(ordbook) => {
              s"""|`lastPrice:${ordbook.payload.lastPrice.getOrElse(0)}`
                  |`closePrice:${ordbook.payload.closePrice.getOrElse(0)}`
                  |`limitUp:${ordbook.payload.limitUp.getOrElse(0)}`
                  |`limitDown:${ordbook.payload.limitDown.getOrElse(0)}`
                  |`tradeStatus:${ordbook.payload.tradeStatus}`
                  |`bids:${ordbook.payload.bids.map(b => s"(${b.price} ${b.quantity})").mkString(",")}`
                  |`asks:${ordbook.payload.asks.map(a => s"(${a.price} ${a.quantity})").mkString(",")}`
                  |""".stripMargin
            }
            case Left(e) => {
              log.error(e.toString)
              s"`Error: ${e.status}`"
            }
          }
        } yield reply
    }
  }

  def doCancelOrder(args: String): F[String] = {
    val orderid = args.split('.').last
    for {
      result <- tInvestRESTApi.cancelOrder(orderid)
      reply = result match {
        case Right(r) => s"`Success`"
        case Left(e) => {
          log.error(e.toString)
          s"`Error: ${e.status}`"
        }
      }
    } yield reply
  }

  private def parseLimitOrderArgs(text: String): Option[(Int, Double, String)] = {
    val args = text.filter(c => c != '/' || c != ' ').split('.')
    args.length match {
      case 4 =>
        val figi = args(1)
        (args(2).toIntOption, args(3).toDoubleOption) match {
          case (lots, price) if lots.isDefined && price.isDefined => Some(lots.get, price.get, figi)
          case (_, _) => None
        }
      case _ => None
    }
  }

  def doLimitOrder(operation: Operation, text: String): F[String] = {
    val parsedArgs = parseLimitOrderArgs(text)
    parsedArgs match {
      case None => s"Wrong command".pure[F]
      case Some(args: (Int, Double, String)) =>
        val (lots, price, figi) = args
        for {
          result <- tInvestRESTApi.limitOrder(FIGI(figi), LimitOrderRequest(lots, operation, price))
          reply = result match {
            case Right(r) => s"`Success: orderId - ${r.payload.orderId}`"
            case Left(e) => {
              log.error(e.toString)
              s"`Error: ${e.status}`"
            }
          }
        } yield reply
    }
  }
}
