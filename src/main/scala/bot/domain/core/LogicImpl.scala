package bot.domain.core

import api.entities.Orders.{LimitOrderRequest, MarketOrderRequest}
import api.entities.{CandleResolution, FIGI, Operation, PlacedOrder}
import api.rest.TInvestRESTApi
import api.websocket.client.TInvestWSApi
import bot.db.DBAccess
import bot.domain.core.controllers._
import bot.domain.{BotOperation, OperationStatus}
import cats.effect.{Sync, Timer}
import cats.implicits.{catsSyntaxApplicativeId, _}
import org.slf4j.Logger

class LogicImpl[F[_] : Sync : Timer](val log: Logger)(implicit tInvestRESTApi: TInvestRESTApi[F],
                                                      tInvestWSApi: TInvestWSApi[F],
                                                      dbAccess: DBAccess[F])
  extends Logic[F] with MarketController[F] with MarketInstrumentController[F] with OrderController[F] with PortfolioController[F] {
  val marketC = new MarketControllertImpl[F](log)
  val orderC = new OrderControllerImpl[F](log)
  val portfolioC = new PortfolioControllerImpl[F](log)
  val instrumentsC = new MarketInstrumentControllerImpl[F](log)

  def helpMsg(): F[String] = {
    s"""
       |
       | Этот бот поможет вам легко  управлять своим портфелем! Воспользуйтесь для этого следующими командами
       |
       |
       |Список базовых команд:
       |/portfolio - Портфель
       |/etflist - Вывести список ETF
       |/currencies - Получение списка валютных пар
       |/orderbook.`figi.depth` - Получение стакана по FIGI
       |/limitOrderBuy.`figi.lots.price` - Лимитная заявка на покупку
       |/limitOrderSell.`figi.lots.price` - Лимитная заявка на продажу
       |/marketOrderBuy.`figi.lots` - Рыночная заявка на покупку
       |/marketOrderSell.`figi.lots` - Рыночная заявка на продажу
       |/cancelOrder.`orderId` - Отмена заявки по OrderId
       |
       |Дополнительные команды:
       |/marketOrderBuy.`figi.lots.stoploss.takeprofit` - Рыночная заявка на покупку с указанными значениями `stoploss` и `takeprofit`
       |/activeOperations - Получить список активных операций
       |/stopOperations - Остановить активные операции
       |""".stripMargin.pure[F]
  }

  override def doMarketOrderCmd(operation: Operation, args: String, userId: Long): F[String] = marketC.doMarketOrderCmd(operation, args, userId)

  override def activeOperations: F[String] = marketC.activeOperations

  override def stopOperations: F[String] = marketC.stopOperations

  override def marketInstrumentMsg(instrument: String): F[String] = instrumentsC.marketInstrumentMsg(instrument)

  override def doOrderbook(args: String): F[String] = orderC.doOrderbook(args)

  override def doCancelOrder(args: String): F[String] = orderC.doCancelOrder(args)

  override def doLimitOrder(operation: Operation, text: String): F[String] = orderC.doLimitOrder(operation, text)

  override def portfolioMsg(): F[String] = portfolioC.portfolioMsg()
}