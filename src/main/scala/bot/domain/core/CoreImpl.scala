package bot.domain.core

import api.rest.TInvestRESTApi
import api.websocket.client.TInvestWSApi
import bot.db.DBAccess
import bot.domain.notifications.NotificationRepo
import cats.effect.{Sync, Timer}
import org.slf4j.LoggerFactory

class CoreImpl[F[_] : Sync : Timer](implicit dbAccess: DBAccess[F],
                     implicit val tInvestRestApi: TInvestRESTApi[F],
                     implicit val tInvestWSApi: TInvestWSApi[F],
                     implicit val notificationRepo: NotificationRepo[F]) extends Core[F] {

  private val logger = LoggerFactory.getLogger("Core")
  private val router = new Router[F](logger)
  private val starter = new Starter[F](logger)
  private val initializer = new Initializer[F](logger)

  override def init(): F[Unit] = initializer.init()

  override def start(): F[Unit] = starter.start()

  override def route(userId: Long, text: String): F[String] = router.handle(userId, text)
}
