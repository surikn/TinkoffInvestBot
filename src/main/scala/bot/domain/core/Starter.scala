package bot.domain.core

import api.entities.Orders.MarketOrderRequest
import api.entities.{CandleResolution, FIGI, Operation}
import api.rest.TInvestRESTApi
import api.websocket.client.TInvestWSApi
import bot.db.DBAccess
import bot.domain.notifications.NotificationRepo
import org.slf4j.Logger
import bot.domain.OperationStatus
import cats.effect.Sync
import bot.domain.notifications.Notification
import cats.implicits._


class Starter[F[_]](val log: Logger)(implicit dbAccess: DBAccess[F],
                                     sync: Sync[F],
                                     tInvestRestApi: TInvestRESTApi[F],
                                     tInvestWSApi: TInvestWSApi[F],
                                     notificationRepo: NotificationRepo[F]) {


  def start(): F[Unit] = {
    for {
      _ <- marketOrderSellByOperation()
    } yield ()
  }

  private def marketOrderSellByOperation(): F[Unit] = {
    for {
      opsE <- dbAccess.getOpsByStatus(OperationStatus.Running)
      _ <- opsE match {
        case Left(e) => Sync[F].delay(log.error("$e"))
        case Right(ops) => {
          ops.traverse {
            op => {
              for {
                orderResult <- tInvestRestApi.marketOrder(op.figi, MarketOrderRequest(op.executedLots, Operation.Sell))
                _ <- orderResult match {
                  case Left(e) => Sync[F].delay(log.error(s"Error market order request - ${e.status} ${e.payload}"))
                  case Right(r) => op.id match {
                    case None => Sync[F].delay(log.error("Unknown operation ID"))
                    case Some(id) =>
                      tInvestWSApi.unsubscribeCandle(op.figi, CandleResolution.`1min`) >>
                        dbAccess.updateOperationStatus(id, OperationStatus.Completed) >>
                        Sync[F].delay(log.info(s"Success market order sell $id ${r.status} ${r.payload}")) >>
                        notificationRepo.push(Notification(op.tgUserId,
                          s"[$id] Заявка на продажу ${op.figi} выполнена"))
                  }
                }
              } yield ()
            }
          }
        }
      }
    } yield ()
  }
}
