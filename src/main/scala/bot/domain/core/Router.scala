package bot.domain.core

import api.entities.Operation
import api.rest.TInvestRESTApi
import api.websocket.client.TInvestWSApi
import bot.db.DBAccess
import cats.effect.{Sync, Timer}
import org.slf4j.Logger
import cats.implicits._



class Router[F[_] : Sync : Timer](val logger: Logger) {
  def handle(userId: Long, text: String)(implicit tInvestRESTApi: TInvestRESTApi[F],
                                                           tInvestWSApi: TInvestWSApi[F],
                                                           dbAccess: DBAccess[F]): F[String] = {
    val logic: LogicImpl[F] = new LogicImpl[F](logger)
    for {
      reply <- text match {
        case "/help" => logic.helpMsg()
        case "/portfolio" => logic.portfolioMsg()
        case "/stocks" => logic.marketInstrumentMsg("stocks")
        case "/bonds" => logic.marketInstrumentMsg("bonds")
        case "/etflist" => logic.marketInstrumentMsg("etfs")
        case "/currencies" => logic.marketInstrumentMsg("currencies")
        case args if args.startsWith("/orderbook.") => logic.doOrderbook(args)
        case args if args.startsWith("/cancelOrder.") => logic.doCancelOrder(args)
        case args if args.startsWith("/limitOrderBuy.") => logic.doLimitOrder(Operation.Buy, args)
        case args if args.startsWith("/limitOrderSell.") => logic.doLimitOrder(Operation.Sell, args)
        case args if args.startsWith("/marketOrderBuy.") => logic.doMarketOrderCmd(Operation.Buy, args, userId)
        case args if args.startsWith("/marketOrderSell.") => logic.doMarketOrderCmd(Operation.Sell, args, userId)
        case "/activeOperations" => logic.activeOperations
        case "/stopOperations" => logic.stopOperations
        case _ => logic.helpMsg()
      }
    } yield reply
  }
}
