package bot.domain.core

trait Core[F[_]] {
  def init(): F[Unit]

  def start(): F[Unit]

  def route(userId: Long, text: String): F[String]
}
