package bot.domain
import api.entities.Operation.{Buy, Sell}
import api.entities.{FIGI, Operation, OrderStatus}
import doobie.Read
import doobie.util.Write
import enumeratum.{CirceEnum, Enum, EnumEntry}
import enumeratum.EnumEntry.Uppercase
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class BotOperation(id: Option[Int] = None,
                        figi: FIGI,
                        stopLoss: Double,
                        takeProfit: Double,
                        operationStatus: OperationStatus,
                        orderId: String,
                        orderStatus: OrderStatus,
                        orderOperation: Operation,
                        requestedLots: Int,
                        executedLots: Int,
                        tgUserId: Long)

object BotOperation {
  implicit val encoder: Encoder[BotOperation] = deriveEncoder[BotOperation]
  implicit val decoder: Decoder[BotOperation] = deriveDecoder[BotOperation]
}


sealed trait OperationStatus extends EnumEntry with Uppercase

case object OperationStatus extends Enum[OperationStatus] with CirceEnum[OperationStatus] {
  override def values: IndexedSeq[OperationStatus] = findValues

  case object Stop extends OperationStatus
  case object Active extends OperationStatus
  case object Running extends OperationStatus
  case object Completed extends OperationStatus

  implicit val operationRead: Read[OperationStatus] =
    Read[String].map {
      case "STOP" => OperationStatus.Stop
      case "ACTIVE" => OperationStatus.Active
      case "RUNNING" => OperationStatus.Running
      case "COMPLETED" => OperationStatus.Completed
    }

  implicit val operationWrite: Write[OperationStatus] =
    Write[String].contramap {
      case Stop => "STOP"
      case Active => "ACTIVE"
      case Running => "Running"
      case Completed => "Completed"
    }

//  override def toString: String = {
//    case Stop => "STOP"
//    case Active => "ACTIVE"
//    case Running => "Running"
//    case Completed => "Completed"
//  }
}