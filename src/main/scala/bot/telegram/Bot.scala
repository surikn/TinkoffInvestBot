package bot.telegram

import cats.effect.{Async, Sync, Timer}
import cats.implicits._
import bot.domain.core.Core
import org.slf4j.{Logger, LoggerFactory}
import telegramium.bots.high.implicits._
import telegramium.bots.high.{Api, LongPollBot, Methods}
import telegramium.bots.{ChatIntId, Markdown, Message}


class Bot[F[_]: Async : Timer](implicit val bot: Api[F], implicit val core: Core[F])
  extends LongPollBot[F](bot) with ToText {

  val log: Logger = LoggerFactory.getLogger("TgBot")

  override def onMessage(msg: Message): F[Unit] = {
    Sync[F].delay {
      log.info(s"got message: $msg")
    } >> (msg match {
      case Text(text) => for {
        reply <- core.route(msg.chat.id, text)
        _ <- send(msg.chat.id, reply)
      } yield ()
      case _ => send(msg.chat.id, "_")
    })
  }

  def send(chatId: Long, text: String): F[Unit] = {
    Methods
      .sendMessage(chatId = ChatIntId(chatId), text = text, parseMode = Some(Markdown))
      .exec
      .void >> Sync[F].delay {
      log.info(s"send message[$chatId]:\n$text")
    }
  }
}
