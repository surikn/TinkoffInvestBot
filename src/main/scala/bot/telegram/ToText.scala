package bot.telegram

import telegramium.bots.Message

trait ToText {
  object Text {
    def unapply(msg: Message): Option[String] = msg.text
  }
}