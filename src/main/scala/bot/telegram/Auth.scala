package bot.telegram

case class Auth() {
  def withToken(token: String) = s"https://api.telegram.org/bot$token"
}
