package bot.db

import api.websocket.response.CandleResponses.CandlePayload

import java.sql.SQLException
import cats.effect.Bracket
import doobie.implicits._
import doobie.util.transactor.Transactor
import bot.domain.{BotOperation, OperationStatus}
import bot.domain.notifications.Notification


class DbAccessImpl[F[_]: Bracket[*[_], Throwable]](transactor: Transactor[F]) extends DBAccess[F] {

  def insertCandle(candle: CandlePayload): F[Either[SQLException, Int]] = {
    CandleQuery.insertCandle(candle).run.transact(transactor).attemptSql
  }

  def insertOperation(operation: BotOperation): F[Either[SQLException, Int]] = {
    OperationQuery.insertOperation(operation).run.transact(transactor).attemptSql
  }

  def updateOperationStatus(id: Int, status: OperationStatus): F[Either[SQLException, Int]] = {
    OperationQuery.updateOperationStatus(id, status).run.transact(transactor).attemptSql
  }

  def getOpsByStatus(operationStatus: OperationStatus): F[Either[SQLException, List[BotOperation]]] = {
    OperationQuery.getOperationsByStatus(operationStatus).transact(transactor).attemptSql
  }

  def getNotifications: F[Either[SQLException, List[Notification]]] = {
    NotificationQuery.getNotifications.transact(transactor).attemptSql
  }

  def insertNotification(notification: Notification): F[Either[SQLException, Int]] = {
    NotificationQuery.insertNotification(notification).run.transact(transactor).attemptSql
  }
}
