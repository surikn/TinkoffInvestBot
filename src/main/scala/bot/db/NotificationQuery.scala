package bot.db

import bot.domain.notifications.Notification
import doobie.implicits.toSqlInterpolator


object NotificationQuery {
  def getNotifications: doobie.ConnectionIO[List[Notification]] = {
    sql"""
         |DELETE FROM notifications RETURNING userId, message
       """
      .stripMargin
      .query[Notification].to[List]
  }

  def insertNotification(notification: Notification): doobie.Update0 = {
    sql"""
         |INSERT INTO notifications (userId, message) VALUES (${notification.userId}, ${notification.message})
       """
      .stripMargin
      .update
  }
}
