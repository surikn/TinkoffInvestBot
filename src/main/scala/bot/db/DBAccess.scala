package bot.db

import api.websocket.response.CandleResponses.CandlePayload
import bot.domain.{BotOperation, OperationStatus}
import bot.domain.notifications.Notification

import java.sql.SQLException


trait DBAccess[F[_]] {
  def insertCandle(candle: CandlePayload): F[Either[SQLException, Int]]

  def insertOperation(operation: BotOperation): F[Either[SQLException, Int]]

  def updateOperationStatus(id: Int, status: OperationStatus): F[Either[SQLException, Int]]

  def getOpsByStatus(operationStatus: OperationStatus): F[Either[SQLException, List[BotOperation]]]

  def getNotifications: F[Either[SQLException, List[Notification]]]

  def insertNotification(notification: Notification): F[Either[SQLException, Int]]
}