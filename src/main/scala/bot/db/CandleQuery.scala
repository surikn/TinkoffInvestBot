package bot.db

import api.websocket.response.CandleResponses.CandlePayload
import doobie.implicits.toSqlInterpolator

object CandleQuery {
  def insertCandle(c: CandlePayload): doobie.Update0 = {
    sql"""
         |INSERT INTO candles ( time, interval, figi, open, close, hight, low, volume )
         |VALUES (${c.time}, ${c.interval}, ${c.figi}, ${c.o}, ${c.c}, ${c.h}, ${c.l}, ${c.v})
       """
      .stripMargin
      .update
  }
}
