package bot.db

import bot.domain.{BotOperation, OperationStatus}
import doobie.implicits.toSqlInterpolator

object OperationQuery {
  def insertOperation(o: BotOperation): doobie.Update0 = {
    // TODO: tostring???
    sql"""
         |INSERT INTO operations ( figi, stopLoss, takeProfit, operationStatus, orderId, orderStatus, orderOperation, requestedLots, executedLots, tgUserId )
         |VALUES (${o.figi}, ${o.stopLoss}, ${o.takeProfit}, ${o.operationStatus.toString}, ${o.orderId}, ${o.orderStatus.toString}, ${o.orderOperation.toString}, ${o.requestedLots}, ${o.executedLots}, ${o.tgUserId})
       """
      .stripMargin
      .update
  }

  def updateOperationStatus(id: Int, status: OperationStatus): doobie.Update0 = {
    // TODO: toString
    sql"""
         |UPDATE operations SET operationStatus = ${status.toString} WHERE ID = $id;
       """
      .stripMargin
      .update
  }

  def getOperationsByStatus(operationStatus: OperationStatus): doobie.ConnectionIO[List[BotOperation]] = {
    sql"""
         |SELECT id, figi, stopLoss, takeProfit, operationStatus, orderId, orderStatus, orderOperation, requestedLots, executedLots, tgUserId
         |FROM operations
         |WHERE operationStatus = ${operationStatus.toString}
       """
      .stripMargin
      .query[BotOperation].to[List]
  }
}
