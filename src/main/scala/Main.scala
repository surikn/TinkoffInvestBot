
import api.rest.{TInvestRESTApi, TInvestRESTApiHtp4s}
import cats.effect.IO.ioConcurrentEffect
import cats.effect.{Blocker, ExitCode, IO, IOApp, Resource}
import cats.implicits.catsSyntaxFlatMapOps
import com.typesafe.scalalogging.LazyLogging
import doobie.ExecutionContexts
import doobie.hikari.HikariTransactor
import fs2.Stream
import bot.config.Config
import bot.db.{DB, DBAccess, DbAccessImpl}
import bot.telegram.{Auth, Bot}
import api.websocket.client.{TInvestWSApi, TInvestWSApiHttp4s, TInvestWSAuthorization, TInvestWSHandler}
import bot.domain.WSHandler
import bot.domain.core.{Core, CoreImpl}
import bot.domain.notifications.{NotificationModel, NotificationRepo, Notifier, TelegramUserNotifier}
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.jdkhttpclient.WSConnectionHighLevel
import telegramium.bots.high.{Api, BotApi}

import scala.concurrent.duration.DurationInt



object Main extends IOApp with LazyLogging {

  type F[+T] = IO[T]

  override def run(args: List[String]): IO[ExitCode] = {
    for {
      configFile <- IO { Option("/home/suriknik/Projects/TinkoffInvestBot/application.conf") }
      config <- Config.load[F](configFile)
      _ <- resources(config).use {
        case (tgHttpClient, tInvestHttpClient, blocker, wsClient, transactor) => {
          implicit val wsHandler: TInvestWSHandler[F] = new WSHandler[F]()
          implicit val notificationRepo: NotificationRepo[F] = new NotificationModel[F]()
          implicit val dbAccess: DBAccess[F] = new DbAccessImpl[F](transactor)
          implicit val tInvestWSApi: TInvestWSApi[F] = new TInvestWSApiHttp4s[F](wsClient, wsHandler)
          implicit val tgBotApi: Api[F] = new BotApi[F](tgHttpClient, Auth().withToken(config.tgBotApiToken), blocker)
          implicit val tgBot: Bot[F] = new Bot[F]()
          implicit val core: Core[F] = new CoreImpl[F]()
          implicit val notifier: Notifier[F] = new TelegramUserNotifier[F]()
          implicit val tInvestApi: TInvestRESTApi[F] = new TInvestRESTApiHtp4s[F](tInvestHttpClient, config.tinkoffInvestApiToken)

          for {
            tInvestWsApiFiber <- tInvestWSApi.listen().start
            tgBotFiber <- tgBot.start().start
            _ <- core.init()
            _ <- (Stream.emit(()) ++ Stream.fixedRate[F](5.second))
              .evalTap {
                _ => {
                  core.start() >>
                    notifier.start()
                }
              }.compile.drain
            _ <- tgBotFiber.join
            _ <- tInvestWsApiFiber.join
          } yield ()
        }
      }
    } yield ExitCode.Success
  }

  def resources(config: Config): Resource[F, (Client[F], Client[F], Blocker, WSConnectionHighLevel[F], HikariTransactor[F])] = {
    import java.net.http.HttpClient
    import org.http4s.client.jdkhttpclient.JdkWSClient

    for {
      wsClient <- JdkWSClient[F](HttpClient.newHttpClient())
        .connectHighLevel(
          TInvestWSAuthorization()
            .withToken(config.tinkoffInvestApiToken)
        )
      httpExecutionContext <- ExecutionContexts.cachedThreadPool[F]
      tgExecutionContext <- ExecutionContexts.cachedThreadPool[F]
      dbEc <- ExecutionContexts.cachedThreadPool[F]
      blocker <- Blocker[F]
      transactor <- DB.transactor(config.database, dbEc, blocker)
      tgHttpClient <- BlazeClientBuilder[F](tgExecutionContext).resource
      tInvestHttpClient <- BlazeClientBuilder[F](httpExecutionContext).resource
    } yield (tgHttpClient, tInvestHttpClient, blocker, wsClient, transactor)
  }
}
