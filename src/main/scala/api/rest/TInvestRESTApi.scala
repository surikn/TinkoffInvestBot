package api.rest

import api.entities.Candles.CandlesResponse
import api.entities.Markets.MarketInstrumentListResponse
import api.entities.Orders.{LimitOrderRequest, MarketOrderRequest, OrderbookResponse, OrdersResponse}
import api.entities.Portfolios.PortfolioResponse
import api.entities.errors.{EmptyResponse, TInvestError}
import api.entities.{CandleResolution, FIGI}

trait TInvestRESTApi[F[_]] {
  def getPortfolio: F[Either[TInvestError, PortfolioResponse]]


  def limitOrder(figi: FIGI, request: LimitOrderRequest): F[Either[TInvestError, OrdersResponse]]


  def marketOrder(figi: FIGI, request: MarketOrderRequest): F[Either[TInvestError, OrdersResponse]]


  def cancelOrder(orderId: String): F[Either[TInvestError, EmptyResponse]]


  def stocks(): F[Either[TInvestError, MarketInstrumentListResponse]]


  def bonds(): F[Either[TInvestError, MarketInstrumentListResponse]]


  def etfs(): F[Either[TInvestError, MarketInstrumentListResponse]]


  def currencies(): F[Either[TInvestError, MarketInstrumentListResponse]]


  def orderbook(figi: FIGI, depth: Int): F[Either[TInvestError, OrderbookResponse]]


  def candles(figi: FIGI, interval: CandleResolution, from: String, to: String): F[Either[TInvestError, CandlesResponse]]
}
