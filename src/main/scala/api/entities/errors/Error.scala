package api.entities.errors

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class TInvestError(trackingId: String, status: String, payload: Payload)
object TInvestError {
  implicit val decoder: Decoder[TInvestError] = deriveDecoder[TInvestError]
  implicit val encoder: Encoder[TInvestError] = deriveEncoder[TInvestError]
}
case class Payload(message: Option[String], code: Option[String])
object Payload {
  implicit val decoder: Decoder[Payload] = deriveDecoder[Payload]
  implicit val encoder: Encoder[Payload] = deriveEncoder[Payload]
}

case class EmptyPayload()
object EmptyPayload {
  implicit val decoder: Decoder[EmptyPayload] = deriveDecoder[EmptyPayload]
  implicit val encoder: Encoder[EmptyPayload] = deriveEncoder[EmptyPayload]
}
case class EmptyResponse(trackingId: String, status: String, payload: EmptyPayload)

object EmptyResponse {
  implicit val decoder: Decoder[EmptyResponse] = deriveDecoder[EmptyResponse]
  implicit val encoder: Encoder[EmptyResponse] = deriveEncoder[EmptyResponse]
}
