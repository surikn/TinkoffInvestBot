package api.entities

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

object Orders {
  case class OrderbookResponse(trackingId: String, status: String, payload: Orderbook)
  object OrderbookResponse {
    implicit val encoder: Encoder[OrderbookResponse] = deriveEncoder[OrderbookResponse]
    implicit val decoder: Decoder[OrderbookResponse] = deriveDecoder[OrderbookResponse]
  }

  case class MarketOrderRequest(lots: Int, operation: Operation)
  object MarketOrderRequest {
    implicit val encoder: Encoder[MarketOrderRequest] = deriveEncoder[MarketOrderRequest]
    implicit val decoder: Decoder[MarketOrderRequest] = deriveDecoder[MarketOrderRequest]
  }

  case class LimitOrderRequest(lots: Int, operation: Operation, price: Double)
  object LimitOrderRequest {
    implicit val encoder: Encoder[LimitOrderRequest] = deriveEncoder[LimitOrderRequest]
    implicit val decoder: Decoder[LimitOrderRequest] = deriveDecoder[LimitOrderRequest]
  }

  case class Orderbook(
      figi: FIGI,
      depth: Int,
      bids: List[OrderResponse],
      asks: List[OrderResponse],
      tradeStatus: TradeStatus,
      minPriceIncrement: Double,
      faceValue: Option[Double],
      lastPrice: Option[Double],
      closePrice: Option[Double],
      limitUp: Option[Double],
      limitDown: Option[Double]
  )
  object Orderbook {
    implicit val encoder: Encoder[Orderbook] = deriveEncoder[Orderbook]
    implicit val decoder: Decoder[Orderbook] = deriveDecoder[Orderbook]
  }

  case class OrderResponse(price: Double, quantity: Int)
  object OrderResponse {
    implicit val encoder: Encoder[OrderResponse] = deriveEncoder[OrderResponse]
    implicit val decoder: Decoder[OrderResponse] = deriveDecoder[OrderResponse]
  }

  case class OrdersResponse(trackingId: String, status: String, payload: PlacedOrder)
  object OrdersResponse {
    implicit val encoder: Encoder[OrdersResponse] = deriveEncoder[OrdersResponse]
    implicit val decoder: Decoder[OrdersResponse] = deriveDecoder[OrdersResponse]
  }
}
