package api.entities

import enumeratum.{CirceEnum, Enum, EnumEntry}
import enumeratum.EnumEntry.Uppercase

sealed trait Currency extends EnumEntry with Uppercase

case object Currency extends Enum[Currency] with CirceEnum[Currency] {
  override def values: IndexedSeq[Currency] = findValues

  case object RUB extends Currency
  case object USD extends Currency
  case object EUR extends Currency
  case object GBP extends Currency
  case object HKD extends Currency
  case object CHF extends Currency
  case object JPY extends Currency
  case object CNY extends Currency
  case object TRY extends Currency

}
