package api.entities

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class PlacedOrder(orderId: String,
                       operation: Operation,
                       status: OrderStatus,
                       rejectReason: Option[String],
                       message: Option[String],
                       requestedLots: Int,
                       executedLots: Int,
                       commission: Option[MoneyAmount])

object PlacedOrder {
  implicit val decoder: Decoder[PlacedOrder] = deriveDecoder[PlacedOrder]
  implicit val encoder: Encoder[PlacedOrder] = deriveEncoder[PlacedOrder]
}

case class Order(
    orderId: String,
    figi: String,
    operation: Operation,
    status: OrderStatus,
    requestedLots: Int,
    executedLots: Int,
    price: Double
)

object Order {
  implicit val decoder: Decoder[Order] = deriveDecoder[Order]
  implicit val encoder: Encoder[Order] = deriveEncoder[Order]
}