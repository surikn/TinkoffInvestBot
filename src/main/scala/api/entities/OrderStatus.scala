package api.entities

import bot.domain.OperationStatus
import bot.domain.OperationStatus.{Active, Completed, Running, Stop}
import doobie.Read
import doobie.util.Write
import enumeratum.{CirceEnum, Enum, EnumEntry}
import enumeratum.EnumEntry.Uppercase

sealed trait OrderStatus extends EnumEntry with Uppercase

case object OrderStatus extends Enum[OrderStatus] with CirceEnum[OrderStatus] {
  override def values: IndexedSeq[OrderStatus] = findValues

  case object New            extends OrderStatus
  case object PartiallyFill  extends OrderStatus
  case object Fill           extends OrderStatus
  case object Cancelled      extends OrderStatus
  case object Replaced       extends OrderStatus
  case object PendingCancel  extends OrderStatus
  case object Rejected       extends OrderStatus
  case object PendingReplace extends OrderStatus
  case object PendingNew     extends OrderStatus

  implicit val operationRead: Read[OrderStatus] =
    Read[String].map {
      case "NEW" => New
      case "PARTIALLY_FILL" => PartiallyFill
      case "FILL" => Fill
      case "CANCELED" => Cancelled
      case "REPLACED" => Replaced
      case "PENDING_CANCEL" => PendingCancel
      case "REJECTED" => Rejected
      case "PENDING_REPLACE" => PendingReplace
      case "PENDING_NEW" => PendingNew
    }

  implicit val operationWrite: Write[OrderStatus] =
    Write[String].contramap {
      case New => "NEW"
      case PartiallyFill => "PARTIALLY_FILL"
      case Fill => "FILL"
      case Cancelled => "CANCELED"
      case Replaced => "REPLACED"
      case PendingCancel => "PENDING_CANCEL"
      case Rejected => "REJECTED"
      case PendingReplace => "PENDING_REPLACE"
      case PendingNew => "PENDING_NEW"
    }

//  override def toString: String = {
//    case New => "NEW"
//    case PartiallyFill => "PARTIALLY_FILL"
//    case Fill => "FILL"
//    case Cancelled => "CANCELED"
//    case Replaced => "REPLACED"
//    case PendingCancel => "PENDING_CANCEL"
//    case Rejected => "REJECTED"
//    case PendingReplace => "PENDING_REPLACE"
//    case PendingNew => "PENDING_NEW"
//  }
}
