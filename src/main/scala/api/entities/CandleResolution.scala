package api.entities

import enumeratum.EnumEntry.Uppercase
import enumeratum._

sealed trait CandleResolution extends EnumEntry with Uppercase

case object CandleResolution extends Enum[CandleResolution] with CirceEnum[CandleResolution] {
  override def values: IndexedSeq[CandleResolution] = findValues

  case object `1min`  extends CandleResolution
  case object `2min`  extends CandleResolution
  case object `3min`  extends CandleResolution
  case object `5min`  extends CandleResolution
  case object `10min` extends CandleResolution
  case object `15min` extends CandleResolution
  case object `30min` extends CandleResolution
  case object hour    extends CandleResolution
  case object day     extends CandleResolution
  case object week    extends CandleResolution
  case object month   extends CandleResolution

}
