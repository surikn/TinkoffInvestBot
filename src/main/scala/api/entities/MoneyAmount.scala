package api.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class MoneyAmount(currency: Currency, value: Double)

object MoneyAmount {
  implicit val encoder: Encoder[MoneyAmount] = deriveEncoder[MoneyAmount]
  implicit val decoder: Decoder[MoneyAmount] = deriveDecoder[MoneyAmount]
}