package api.entities

import enumeratum.{CirceEnum, Enum, EnumEntry}
import enumeratum.EnumEntry.Uppercase

sealed trait TradeStatus extends EnumEntry with Uppercase

case object TradeStatus extends Enum[TradeStatus] with CirceEnum[TradeStatus] {
  override def values: IndexedSeq[TradeStatus] = findValues

  case object NormalTrading          extends TradeStatus
  case object NotAvailableForTrading extends TradeStatus
}
