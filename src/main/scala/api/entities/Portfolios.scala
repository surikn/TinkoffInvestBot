package api.entities

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

object Portfolios {
  case class PortfolioResponse(trackingId: String, payload: Portfolio, status: String)
  object PortfolioResponse {
    implicit val encoder: Encoder[PortfolioResponse] = deriveEncoder[PortfolioResponse]
    implicit val decoder: Decoder[PortfolioResponse] = deriveDecoder[PortfolioResponse]
  }


  case class Portfolio(positions: Seq[PortfolioPosition])
  object Portfolio {
    implicit val encoder: Encoder[Portfolio] = deriveEncoder[Portfolio]
    implicit val decoder: Decoder[Portfolio] = deriveDecoder[Portfolio]
  }

  case class PortfolioPosition(
      figi: FIGI,
      ticker: Option[String],
      isin: Option[String],
      instrumentType: String,
      balance: Double,
      blocked: Option[Double],
      expectedYield: Option[MoneyAmount],
      lots: Int,
      averagePositionPrice: Option[MoneyAmount],
      averagePositionPriceNoNkd: Option[MoneyAmount],
      name: String
  )
  object PortfolioPosition {
    implicit val encoder: Encoder[PortfolioPosition] = deriveEncoder[PortfolioPosition]
    implicit val decoder: Decoder[PortfolioPosition] = deriveDecoder[PortfolioPosition]
  }
}
