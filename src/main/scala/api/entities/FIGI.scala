package api.entities

import doobie.{Get, Put}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder, KeyEncoder}

case class FIGI(value: String)

object FIGI {
  implicit val encoder: Encoder[FIGI] = deriveEncoder[FIGI]
  implicit val decoder: Decoder[FIGI] = deriveDecoder[FIGI]
  implicit val figiGet: Get[FIGI] = Get[String].map(FIGI(_))
  implicit val figiPut: Put[FIGI] = Put[String].contramap(_.value)
}