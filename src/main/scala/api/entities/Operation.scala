package api.entities

import doobie.Read
import doobie.util.Write
import enumeratum.{CirceEnum, Enum, EnumEntry}
import enumeratum.EnumEntry.Uppercase

sealed trait Operation extends EnumEntry with Uppercase

case object Operation extends Enum[Operation] with CirceEnum[Operation] {
  override def values: IndexedSeq[Operation] = findValues

  case object Buy extends Operation

  case object Sell extends Operation

  implicit val operationRead: Read[Operation] =
    Read[String].map {
      case "BUY" => Operation.Buy
      case "SELL" => Operation.Sell
    }

  implicit val operationWrite: Write[Operation] =
    Write[String].contramap {
      case Buy => "BUY"
      case Sell => "SELL"
    }
}
