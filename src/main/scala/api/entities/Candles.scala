package api.entities

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

object Candles {
  case class CandlesResponse(trackingId: String, status: String, payload: Candles)
  object CandlesResponse {
    implicit val decoder: Decoder[CandlesResponse] = deriveDecoder[CandlesResponse]
    implicit val encoder: Encoder[CandlesResponse] = deriveEncoder[CandlesResponse]
  }

  case class Candles(figi: FIGI, interval: CandleResolution, candles: List[Candle])
  object Candles {
    implicit val decoder: Decoder[Candles] = deriveDecoder[Candles]
    implicit val encoder: Encoder[Candles] = deriveEncoder[Candles]
  }

  case class Candle(
  figi: FIGI,
  interval: CandleResolution,
  o: Double,
  c: Double,
  h: Double,
  l: Double,
  v: Int,
  time: String
  )
  object Candle {
    implicit val decoder: Decoder[Candle] = deriveDecoder[Candle]
    implicit val encoder: Encoder[Candle] = deriveEncoder[Candle]
  }
}
