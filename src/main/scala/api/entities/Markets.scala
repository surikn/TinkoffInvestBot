package api.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

object Markets {

  case class MarketInstrument(
      figi: FIGI,
      ticker: String,
      isin: Option[String],
      minPriceIncrement: Option[Double],
      lot: Int,
      minQuantity: Option[Int],
      currency: Option[String],
      name: String,
      `type`: String
  )
  object MarketInstrument {
    implicit val decoder: Decoder[MarketInstrument] = deriveDecoder[MarketInstrument]
    implicit val encoder: Encoder[MarketInstrument] = deriveEncoder[MarketInstrument]
  }

  case class MarketInstrumentList(total: Int, instruments: List[MarketInstrument])
  object MarketInstrumentList {
    implicit val decoder: Decoder[MarketInstrumentList] = deriveDecoder[MarketInstrumentList]
    implicit val encoder: Encoder[MarketInstrumentList] = deriveEncoder[MarketInstrumentList]
  }

  case class MarketInstrumentListResponse(trackingId: String, status: String, payload: MarketInstrumentList)
  object MarketInstrumentListResponse {
    implicit val decoder: Decoder[MarketInstrumentListResponse] = deriveDecoder[MarketInstrumentListResponse]
    implicit val encoder: Encoder[MarketInstrumentListResponse] = deriveEncoder[MarketInstrumentListResponse]
  }

}
