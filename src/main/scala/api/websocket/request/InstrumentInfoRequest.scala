package api.websocket.request

import api.entities.FIGI
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class InstrumentInfoRequest(event: String, figi: FIGI, request_id: Option[String] = None) extends TInvestWSRequest

object InstrumentInfoRequest {
  implicit val encoder: Encoder[InstrumentInfoRequest] = deriveEncoder[InstrumentInfoRequest]
  implicit val decoder: Decoder[InstrumentInfoRequest] = deriveDecoder[InstrumentInfoRequest]
}
