package api.websocket.request

import api.entities.FIGI
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class OrderBookRequest(event: String, figi: FIGI, depth: Int, request_id: Option[String] = None)
    extends TInvestWSRequest

object OrderBookRequest {
  implicit val encoder: Encoder[OrderBookRequest] = deriveEncoder[OrderBookRequest]
  implicit val decoder: Decoder[OrderBookRequest] = deriveDecoder[OrderBookRequest]
}
