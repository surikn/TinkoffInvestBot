package api.websocket.request

import api.entities.FIGI
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class CandleRequest(event: String, figi: FIGI, interval: String, request_id: Option[String] = None)
    extends TInvestWSRequest

object CandleRequest {
  implicit val decoder: Decoder[CandleRequest] = deriveDecoder[CandleRequest]
  implicit val encoder: Encoder[CandleRequest] = deriveEncoder[CandleRequest]
}
