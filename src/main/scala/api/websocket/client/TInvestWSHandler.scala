package api.websocket.client

import api.websocket.response.TInvestWSResponse

trait TInvestWSHandler[F[_]] {
  def handle(response: TInvestWSResponse): F[Unit]
}