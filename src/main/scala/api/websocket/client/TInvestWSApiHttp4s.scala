package api.websocket.client

import api.entities.{CandleResolution, FIGI}
import api.websocket.request.{CandleRequest, InstrumentInfoRequest, OrderBookRequest}
import api.websocket.response.CandleResponses.CandleResponse
import api.websocket.response.InstrumentResponses.InstrumentInfoResponse
import api.websocket.response.OrderBookResponses.OrderBookResponse
import api.websocket.response.TInvestWSResponse
import cats.Applicative.ops.toAllApplicativeOps
import cats.effect.{Concurrent, ConcurrentEffect, ContextShift, Timer}
import io.circe.generic.auto.{exportDecoder, exportEncoder}
import io.circe.syntax.EncoderOps
import io.circe.{Decoder, Encoder, jawn}
import org.http4s.client.jdkhttpclient.{WSConnectionHighLevel, WSFrame}


class TInvestWSApiHttp4s[F[_] : ConcurrentEffect : Timer : ContextShift : Concurrent](
                                                                                       connection: WSConnectionHighLevel[F],
                                                                                       handler: TInvestWSHandler[F]
                                                                                     ) extends TInvestWSApi[F] {


  override def subscribeCandle(figi: FIGI, interval: CandleResolution): F[Unit] = {
    connection.send {
      WSFrame.Text {
        CandleRequest("candle:subscribe", figi, interval.toString).asJson.noSpaces
      }
    }
  }

  override def subscribeOrderbook(figi: FIGI, depth: Int): F[Unit] = {
    connection.send {
      WSFrame.Text {
        OrderBookRequest("orderbook:subscribe", figi, depth).asJson.noSpaces
      }
    }
  }

  override def subscribeInstrumentInfo(figi: FIGI): F[Unit] = {
    connection.send {
      WSFrame.Text {
        InstrumentInfoRequest("instrument_info:subscribe", figi).asJson.noSpaces
      }
    }
  }

  override def unsubscribeCandle(figi: FIGI, interval: CandleResolution): F[Unit] = {
    connection.send {
      WSFrame.Text {
        CandleRequest("candle:subscribe", figi, interval.toString).asJson.noSpaces
      }
    }
  }

  override def unsubscribeOrderbook(figi: FIGI, depth: Int): F[Unit] = {
    connection.send {
      WSFrame.Text {
        OrderBookRequest("orderbook:unsubscribe", figi, depth).asJson.noSpaces
      }
    }
  }

  override def unsubscribeInstrumentInfo(figi: FIGI): F[Unit] = {
    connection.send {
      WSFrame.Text {
        InstrumentInfoRequest("instrument_info:unsubscribe", figi).asJson.noSpaces
      }
    }
  }

  def listen(): F[List[TInvestWSResponse]] = {
    connection.receiveStream.collect { case WSFrame.Text(str, _) => jawn.decode[TInvestWSResponse](str) }.collect {
      case Right(response) => response
    }.evalTap { data => handler.handle(data) }.compile.toList
  }

  implicit val decodeTInvestWSResponse: Decoder[TInvestWSResponse] =
    List[Decoder[TInvestWSResponse]](
      Decoder[CandleResponse].widen,
      Decoder[OrderBookResponse].widen,
      Decoder[InstrumentInfoResponse].widen
    ).reduceLeft(_ or _)

  implicit val encodeTInvestWSResponse: Encoder[TInvestWSResponse] = Encoder.instance {
    case cr@CandleResponse(_, _, _) => cr.asJson
    case or@OrderBookResponse(_, _, _) => or.asJson
    case ir@InstrumentInfoResponse(_, _, _) => ir.asJson
  }
}