package api.websocket.client

import api.entities.{CandleResolution, FIGI}
import api.websocket.response.TInvestWSResponse

trait TInvestWSApi[F[_]] {


  def subscribeCandle(figi: FIGI, interval: CandleResolution): F[Unit]


  def subscribeOrderbook(figi: FIGI, depth: Int): F[Unit]

  def subscribeInstrumentInfo(figi: FIGI): F[Unit]

  def unsubscribeCandle(figi: FIGI, interval: CandleResolution): F[Unit]


  def unsubscribeOrderbook(figi: FIGI, depth: Int): F[Unit]


  def unsubscribeInstrumentInfo(figi: FIGI): F[Unit]

  def listen(): F[List[TInvestWSResponse]]
}


