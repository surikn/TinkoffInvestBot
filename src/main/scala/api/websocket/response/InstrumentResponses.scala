package api.websocket.response

import api.entities.FIGI
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

object InstrumentResponses {
  case class InstrumentInfoResponse(event: String, time: String, payload: InstrumentInfoPayload)
      extends TInvestWSResponse
  object InstrumentInfoResponse {
    implicit val encoder: Encoder[InstrumentInfoResponse] = deriveEncoder[InstrumentInfoResponse]
    implicit val decoder: Decoder[InstrumentInfoResponse] = deriveDecoder[InstrumentInfoResponse]
  }

  case class InstrumentInfoPayload(
      trade_status: String,
      min_price_increment: Double,
      lot: Double,
      accrued_interest: Option[Double],
      limit_up: Option[Double],
      limit_down: Option[Double],
      figi: FIGI
  )
  object InstrumentInfoPayload {
    implicit val encoder: Encoder[InstrumentInfoPayload] = deriveEncoder[InstrumentInfoPayload]
    implicit val decoder: Decoder[InstrumentInfoPayload] = deriveDecoder[InstrumentInfoPayload]
  }
}
