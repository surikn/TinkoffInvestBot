package api.websocket.response

import api.entities.FIGI
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

object CandleResponses {
  case class CandleResponse(event: String, time: String, payload: CandlePayload) extends TInvestWSResponse
  object CandleResponse {
    implicit val decoder: Decoder[CandleResponse] = deriveDecoder[CandleResponse]
    implicit val encoder: Encoder[CandleResponse] = deriveEncoder[CandleResponse]
  }

  case class CandlePayload(
      o: Double,
      c: Double,
      h: Double,
      l: Double,
      v: Double,
      time: String,
      interval: String,
      figi: FIGI
  )
  object CandlePayload {
    implicit val decoder: Decoder[CandlePayload] = deriveDecoder[CandlePayload]
    implicit val encoder: Encoder[CandlePayload] = deriveEncoder[CandlePayload]
  }
}
