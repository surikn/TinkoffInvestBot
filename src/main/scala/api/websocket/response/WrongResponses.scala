package api.websocket.response

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

object WrongResponses {
  case class WrongResponse(
      event: String,
      time: String,
      payload: WrongResponsePayload
  )

  case class WrongResponsePayload(error: String, request_id: Option[String])
  object WrongResponsePayload {
    implicit val encoder: Encoder[WrongResponsePayload] = deriveEncoder[WrongResponsePayload]
    implicit val decoder: Decoder[WrongResponsePayload] = deriveDecoder[WrongResponsePayload]
  }
}
