package api.websocket.response

import api.entities.FIGI
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

object OrderBookResponses {

  case class OrderBookResponse(
      event: String,
      time: String,
      payload: OrderBookPayload
  ) extends TInvestWSResponse

  object OrderBookResponse {
    implicit val encoder: Encoder[OrderBookResponse] = deriveEncoder[OrderBookResponse]
    implicit val decoder: Decoder[OrderBookResponse] = deriveDecoder[OrderBookResponse]
  }

  case class OrderBookPayload(depth: Int, bids: Seq[(Double, Double)], asks: Seq[(Double, Double)], figi: FIGI)
  object OrderBookPayload {
    implicit val encoder: Encoder[OrderBookPayload] = deriveEncoder[OrderBookPayload]
    implicit val decoder: Decoder[OrderBookPayload] = deriveDecoder[OrderBookPayload]
  }
}
