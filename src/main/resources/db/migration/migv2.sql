CREATE TABLE candles (id SERIAL, time TEXT, interval TEXT, figi TEXT, open REAL, close REAL, hight REAL, low REAL, volume REAL);

CREATE TABLE operations (id SERIAL, figi TEXT, stopLoss REAL, takeProfit REAL,
                         time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                         operationStatus TEXT, orderId TEXT, orderStatus TEXT, orderOperation TEXT,
                         requestedLots INT, executedLots INT, tgUserId bigint);

CREATE TABLE notifications (id SERIAL, userId INT, message TEXT);